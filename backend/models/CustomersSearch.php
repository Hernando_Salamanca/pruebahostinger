<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Customers;

/**
 * CustomersSearch represents the model behind the search form of `common\models\Customers`.
 */
class CustomersSearch extends Customers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'rc', 'category', 'num_doc', 'association'], 'integer'],
            [['name', 'last_name', 'type_doc', 'addres', 'phone', 'email', 'eps', 'afp', 'arl', 'ccf', 'office', 'user', 'key_system', 'date_register'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rc' => $this->rc,
            'category' => $this->category,
            'num_doc' => $this->num_doc,
            'association' => $this->association,
            'date_register' => $this->date_register,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'type_doc', $this->type_doc])
            ->andFilterWhere(['like', 'addres', $this->addres])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'eps', $this->eps])
            ->andFilterWhere(['like', 'afp', $this->afp])
            ->andFilterWhere(['like', 'arl', $this->arl])
            ->andFilterWhere(['like', 'ccf', $this->ccf])
            ->andFilterWhere(['like', 'office', $this->office]) 
            ->andFilterWhere(['like', 'user', $this->user])
            ->andFilterWhere(['like', 'key_system', $this->key_system]);

        return $dataProvider;
    }
}
