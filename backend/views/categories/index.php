<?php

use common\models\Categories;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\CategoriesSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Categorías';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <p>
        <?= Html::a('Crear Categorías', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            [
                'attribute' => 'dependence',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    if ($model->dependence == 1) {
                        return 'Dependiente';
                    }
                    if ($model->dependence == 0) {
                        return 'Independiente';
                    }
                },
                'filter' => [
                    1 => 'Dependiente',
                    0 => 'Independiente'
                ]
            ],
            'description',
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, Categories $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'id' => $model->id]);
            //      }
            // ],
            [
                'class' => 'yii\grid\ActionColumn',
                // 'visibleButtons' =>
                // [
                //     'update' => Yii::$app->user->can('SuperAdministrador')
                // ],
                'contentOptions' =>
                [
                    'style' => 'width:100px;text-align: center; vertical-align: middle; white-space: nowrap;'
                ],
                'template' => '{view} {update} {delete}'
            ],
        ],
    ]); ?>


</div>
