<?php

use common\models\Associations;
use common\models\Categories;
use common\models\Customers;
use common\models\Payments;

$count_customers = Customers::find()->count();
$count_categories = Categories::find()->count();
$count_associations = Associations::find()->count();
$count_payments = Payments::find()->count();
?>
<?php
$this->title = 'Página de Inicio';
$this->params['breadcrumbs'] = [['label' => $this->title]];
?>
<div class="container-fluid">
    <!-- <div class="row">
        <div class="col-lg-6">
            <?= \hail812\adminlte\widgets\Alert::widget([
                'type' => 'success',
                'body' => '<h3>Gestionar social de Colombia S.A.S!</h3>',
            ]) ?>
        </div>
    </div> -->

    <div class="row">
        <div class="col-lg-2 col-md-3 col-sm-3 col-6">
            <?= \hail812\adminlte\widgets\SmallBox::widget([
                'title' => $count_categories,
                'text' => 'Categorías',
                'icon' => 'fas fa-shopping-cart',
            ]) ?>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-3 col-6">
            <?= \hail812\adminlte\widgets\SmallBox::widget([
                'title' => $count_associations,
                'text' => 'Gremios',
                'icon' => 'fas fa-shopping-cart',
            ]) ?>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <?php $smallBox = \hail812\adminlte\widgets\SmallBox::begin([
                'title' => $count_customers,
                'text' => 'Clientes',
                'icon' => 'fas fa-shopping-cart',
                'theme' => 'success'
            ]) ?>
            <?php \hail812\adminlte\widgets\SmallBox::end() ?>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <?php $smallBox = \hail812\adminlte\widgets\SmallBox::begin([
                'title' => $count_payments,
                'text' => 'Pagos',
                'icon' => 'fas fa-shopping-cart',
                'theme' => 'danger'
            ]) ?>
            <?php \hail812\adminlte\widgets\SmallBox::end() ?>
        </div>
    </div>
</div>