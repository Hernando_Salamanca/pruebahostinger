<footer class="main-footer">
    <strong>Copyright &copy; 2023 <i style="color:blue">Gestionar Social de Colombia</i>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Created by HST in Yii Version</b> 3.1.0
    </div>
</footer>