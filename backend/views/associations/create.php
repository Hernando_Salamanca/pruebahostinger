<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Associations $model */

$this->title = 'Crear Gremios';
$this->params['breadcrumbs'][] = ['label' => 'Gremios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="associations-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
