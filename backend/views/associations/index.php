<?php

use common\models\Associations;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\AssociationsSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Gremios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="associations-index">

    <p>
        <?= Html::a('Crear Gremios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'description',
            [
                'class' => 'yii\grid\ActionColumn',
                // 'visibleButtons' =>
                // [
                //     'update' => Yii::$app->user->can('SuperAdministrador')
                // ],
                'contentOptions' =>
                [
                    'style' => 'width:100px;text-align: center; vertical-align: middle; white-space: nowrap;'
                ],
                'template' => '{view} {update} {delete}'
            ],
        ],
    ]); ?>


</div>
