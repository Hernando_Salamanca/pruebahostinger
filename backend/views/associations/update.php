<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Associations $model */

$this->title = 'Modificar Gremio: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Gremios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="associations-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
