<?php

use common\models\Customers;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

/** @var yii\web\View $this */
/** @var common\models\Payments $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="payments-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'customer_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Customers::find()->all(), 'id', function ($model) {
                    return $model->num_doc . ' - ' . $model->name . ' ' . $model->last_name;
                }),
                'options' => ['placeholder' => 'Seleccione ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'month')->dropDownList(
                [
                    'Enero' => 'Enero',
                    'Febrero' => 'Febrero',
                    'Marzo' => 'Marzo',
                    'Abril' => 'Abril',
                    'Mayo' => 'Mayo',
                    'Junio' => 'Junio',
                    'Julio' => 'Julio',
                    'Agosto' => 'Agosto',
                    'Septiembre' => 'Septiembre',
                    'Octubre' => 'Octubre',
                    'Noviembre' => 'Noviembre',
                    'Diciembre' => 'Diciembre',
                ],
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'base')->textInput(['value' => 1160000]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'factors')->dropDownList(
                [
                    1 => 'Eps',
                    2 => 'Eps + R4',
                    3 => 'Eps + Pensión',
                    4 => 'Eps + Pensión + Riesgo',
                    5 => 'Eps + Pensión + Riesgo  + Ccf',
                    6 => 'Arl',
                ],
                ['prompt' => 'Seleccione', 'id' => 'factors', 'onchange' => 'hideshow();']
            ) ?>
        </div>
        <div class="col-md-4" id="campo1" style="display: none;">
            <?= $form->field($model, 'risk_level')->dropDownList(
                [
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => 5
                ],
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-4" id="campo2" style="display: none;">
            <?= $form->field($model, 'risk_level_2')->dropDownList(
                [
                    4 => 4
                ],
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-4" id="campo3" style="display: none;">
            <?= $form->field($model, 'risk_level_3')->dropDownList(
                [
                    4 => 4,
                    5 => 5
                ],
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'admin')->textInput(['value' => 20000]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'interest')->textInput(['value' => 0]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    function hideshow() {
        var selectedOption = $("#factors").val();
        // Oculta todos los campos
        $("#campo1").hide();
        $("#campo2").hide();
        // Muestra el campo correspondiente según la opción seleccionada
        if (selectedOption == 1) { //factor == EPS          
        } else if (selectedOption == 2) { //factor == EPS
            $("#campo2").show();
        } else if (selectedOption == 3) { //factor == EPS + Pensión
        } else if (selectedOption == 4) { //facor == Eps + Pensión + Riesgo
            $("#campo1").show();
        } else if (selectedOption == 5) { //factor == Eps + Pensión + Riesgo  + Ccf
            $("#campo1").show();
        } else if (selectedOption == 6) { //factor == Arl
            $("#campo3").show();
        }
    }
    // Ejecut la función al cargar la página si hay una opción seleccionada inicialmente
    $(document).ready(function() {
        hideshow();
    });
</script>