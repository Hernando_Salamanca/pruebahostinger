<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var common\models\Payments $model */

$this->title = 'Pago del mes de ' . $model->month . ' de: ' . $model->customer->name;
$this->params['breadcrumbs'][] = ['label' => 'Pagos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="payments-view">
    <div class="row">
        <div class="col-md-6 text-left">
            <p>
                <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Está seguro que desea eliminar este item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        </div>
        <div class="col-md-6 text-right">
            <p>
                <?= Html::a('Nuevo Pago', ['create'], ['class' => 'btn btn-success', 'style' => 'align']) ?>
            </p>
        </div>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'customer_id',
                'label' => 'Cliente',
                'value' => function ($model) {
                    return $model->customer->name . ' ' . $model->customer->last_name;
                },
            ],
            'month',
            [
                'attribute' => 'base',
                'value' => function ($model) {
                    return '$ ' . number_format($model->base);
                },
            ],
            [
                'attribute' => 'factors',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    if ($model->factors == 1) {
                        return 'Eps';
                    }
                    if ($model->factors == 2) {
                        return 'Eps + R4';
                    }
                    if ($model->factors == 3) {
                        return 'Eps + Pensión';
                    }
                    if ($model->factors == 4) {
                        return 'Eps + Pensión + Riesgo';
                    }
                    if ($model->factors == 5) {
                        return 'Eps + Pensión + Riesgo  + Ccf';
                    }
                    if ($model->factors == 6) {
                        return 'Arl';
                    }
                }
            ],
            [
                'label' => 'Nivel de Riesgo',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    if ($model->risk_level != null) {
                        return $model->risk_level;
                    }
                    if ($model->risk_level_2 != null) {
                        return $model->risk_level_2;
                    }
                    if ($model->risk_level_3 != null) {
                        return $model->risk_level_3;
                    }
                }
            ],
            [
                'attribute' => 'admin',
                'value' => function ($model) {
                    return '$ ' . number_format($model->admin);
                },
            ],
            [
                'attribute' => 'interest',
                'value' => function ($model) {
                    return '$ ' . number_format($model->interest);
                },
            ],
            [
                'attribute' => 'date_liquidation',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->date_liquidation, 'long');
                },
            ],
            [
                'attribute' => 'real_value',
                'value' => function ($model) {
                    return '$ ' . number_format($model->real_value);
                },
            ],
            [
                'attribute' => 'paid_value',
                'value' => function ($model) {
                    return '$ ' . number_format($model->paid_value);
                },
            ],
        ],
    ]) ?>

</div>