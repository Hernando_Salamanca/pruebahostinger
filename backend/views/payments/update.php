<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Payments $model */

$this->title = 'Modificar Pago de : ' . $model->customer->name;
$this->params['breadcrumbs'][] = ['label' => 'Pago', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payments-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
