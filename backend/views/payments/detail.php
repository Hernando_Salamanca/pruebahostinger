<style>
    .table1 {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        margin-bottom: 20px;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<?php

use common\helpers\Core;
use common\models\Payments;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var backend\models\PaymentsSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Pagos de ' . $model->num_doc;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-index">
   
<div class="col-md-11 mx-auto">
        <table class="table1">
            <tr>
                <th>Rc</th>
                <th>Nombre</th>
                <th>Categoría</th>
                <th>Gremio</th>
            </tr>
            <tr>
                <td><?= $model->rc ?></td>
                <td><?= $model->name . ' ' . $model->last_name; ?></td>
                <td><?= $model->category0->name?></td>
                <td><?= $model->association0->name ?></td>
            </tr>
        </table>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'customer_id',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    return $model->customer->name . ' ' . $model->customer->last_name;
                },
                'filter' => ArrayHelper::map(Core::getCustomers(), 'id', 'name'),
            ],
            [
                'attribute' => 'month',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    return $model->month;
                },
                'filter' => [
                    'Enero' => 'Enero',
                    'Febrero' => 'Febrero',
                    'Marzo' => 'Marzo',
                    'Abril' => 'Abril',
                    'Mayo' => 'Mayo',
                    'Junio' => 'Junio',
                    'Julio' => 'Julio',
                    'Agosto' => 'Agosto',
                    'Septiembre' => 'Septiembre',
                    'Octubre' => 'Octubre',
                    'Noviembre' => 'Noviembre',
                    'Diciembre' => 'Diciembre',
                ],
            ],
            [
                'attribute' => 'base',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    return '$ ' . number_format($model->base);
                },
            ],
            [
                'attribute' => 'factors',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    if ($model->factors == 1) {
                        return 'Eps';
                    }
                    if ($model->factors == 2) {
                        return 'Eps + R4';
                    }
                    if ($model->factors == 3) {
                        return 'Eps + Pensión';
                    }
                    if ($model->factors == 4) {
                        return 'Eps + Pensión + Riesgo';
                    }
                    if ($model->factors == 5) {
                        return 'Eps + Pensión + Riesgo  + Ccf';
                    }
                    if ($model->factors == 6) {
                        return 'Arl';
                    }
                },
                'filter' => [
                    1 => 'Eps',
                    2 => 'Eps + R4',
                    3 => 'Eps + Pensión',
                    4 => 'Eps + Pensión + Riesgo',
                    5 => 'Eps + Pensión + Riesgo  + Ccf',
                    6 => 'Arl',
                ]
            ],
            [
                'attribute' => 'risk_level',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    return $model->risk_level;
                },
                'filter' => [
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5'
                ],
            ],
            [
                'attribute' => 'date_liquidation',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    return $model->date_liquidation;
                },
            ],
            [
                'attribute' => 'real_value',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    return '$ ' . number_format($model->real_value);
                },
            ],
            //'paid_value',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' =>
                [
                    'style' => 'width:100px;text-align: center; vertical-align: middle; white-space: nowrap;'
                ],
                'template' => '{view} {update} {delete}'
            ],
        ],
    ]); ?>


</div>