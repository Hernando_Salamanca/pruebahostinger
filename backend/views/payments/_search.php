<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var backend\models\PaymentsSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="payments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'month') ?>

    <?= $form->field($model, 'base') ?>

    <?= $form->field($model, 'factors') ?>

    <?php // echo $form->field($model, 'risk_level') 
    ?>

    <?php // echo $form->field($model, 'admin') 
    ?>

    <?php // echo $form->field($model, 'interest') 
    ?>
    <?php // echo $form->field($model, 'admin') 
    ?>

    <?php // echo $form->field($model, 'interest') 
    ?>

    <?php // echo $form->field($model, 'date_liquidation') 
    ?>

    <?php // echo $form->field($model, 'real_value') 
    ?>

    <?php // echo $form->field($model, 'paid_value') 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>