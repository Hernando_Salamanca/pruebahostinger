<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Payments $model */

$this->title = 'Registrar Pago';
$this->params['breadcrumbs'][] = ['label' => 'Pagos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
