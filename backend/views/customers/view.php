<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var common\models\Customers $model */

$this->title = 'Cliente: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="customers-view">
    <p>
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro que desea eliminar este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'rc',
            [
                'attribute' => 'category',
                'value' => function ($model) {
                    return $model->category0->name;
                },
            ],
            'name',
            'last_name',
            'type_doc',
            'num_doc',
            'addres',
            'phone',
            'email:email',
            'eps',
            'afp',
            'arl',
            'ccf',
            [
                'attribute' => 'association',
                'value' => function ($model) {
                    return $model->association0->name;
                },
            ],
            'office',
            'user',
            'key_system',
            'date_register',
        ],
    ]) ?>

</div>