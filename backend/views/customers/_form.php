<?php

use common\helpers\Core;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Customers $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="customers-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'rc')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'category')->dropDownList(
                ArrayHelper::map(Core::getCategories(), 'id', 'name'),
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'type_doc')->dropDownList(
                [
                    'NIT' => 'NIT',
                    'CC' => 'CC',
                    'CE' => 'CE',
                    'PT' => 'PT',
                ],
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'num_doc')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'addres')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'eps')->dropDownList(
                [
                    'Compensar' => 'Compensar',
                    'Convida' => 'Convida',
                    'Ecopsos' => 'Ecopsos',
                    'Famisanar' => 'Famisanar',
                    'Fosyga Regimen de Excepción' => 'Fosyga Regimen de Excepción',
                    'Nueva EPS' => 'Nueva EPS',
                    'Salud Total' => 'Salud Total',
                    'Sanitas LTDA' => 'Sanitas LTDA',
                ],
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'afp')->dropDownList(
                [
                    'Colfondos' => 'Colfondos',
                    'Porvenir' => 'Porvenir',
                    'Protección' => 'Protección',
                    'Skandia' => 'Skandia',                    
                    'Colpensiones' => 'Colpensiones',                    
                    'Ninguno' => 'Ninguno'
                ],
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'arl')->dropDownList(
                [
                    'Alfa' => 'Alfa',                    
                    'Colmena' => 'Colmena',                    
                    'Colpatria' => 'Colpatria',
                    'La Equidad' => 'La Equidad',
                    'Mapfre' => 'Mapfre',                    
                    'Positiva' => 'Positiva',                    
                    'Seguros Bolivar S.A.' => 'Seguros Bolivar S.A.',
                    'Suramericana' => 'Suramericana',
                    'Ninguna' => 'Ninguna',
                ],
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'ccf')->dropDownList(
                [
                    'Cafam' => 'Cafam',
                    'Colsubsidio' => 'Colsubsidio',                
                    'Compensar' => 'Compensar',
                    'Ninguna' => 'Ninguna',
                ],
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'association')->dropDownList(
                ArrayHelper::map(Core::getAssociations(), 'id', 'name'),
                ['prompt' => 'Seleccione']
            ) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'office')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'user')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'key_system')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
        <div class="form-group text-center">
            <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']) ?>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>