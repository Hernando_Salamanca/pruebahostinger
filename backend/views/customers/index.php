<?php

use common\helpers\Core;
use common\models\Customers;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var backend\models\CustomersSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customers-index">

    <p>
        <?= Html::a('Crear Clientes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'rc',
            'num_doc',
            'name',
            'last_name',
            [
                'attribute' => 'category',
                'value' => function ($model) {
                    return $model->category0->name;
                },
                'filter' => ArrayHelper::map(Core::getCategories(), 'id', 'name'),
            ],
            //'type_doc',
            //'addres',
            //'phone',
            //'email:email',
            'eps',
            'afp',
            'arl',
            'ccf',
            [
                'attribute' => 'association',
                //'contentOptions' => ['style' => 'vertical-align: middle;'],
                'value' => function ($model) {
                    return $model->association0->name;
                },
                'filter' => ArrayHelper::map(Core::getAssociations(), 'id', 'name'),
            ],
            //'user',
            //'key_system',
            //'date_register',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' =>
                [
                    'style' => 'width: 300 px; text-align: center; vertical-align: middle; white-space: nowrap;'
                ],
                'template' => '{view} {update} {delete} ',
                'buttons' => [
                    'pagos' => function ($url, $model) {
                        return Html::a('<i class="fa fa-coins";></i>', Url::to(['/payments/detail', 'id' => $model->id]), ['title' => 'Pagos']);
                    }
                ],
            ],
        ],
    ]); ?>


</div>