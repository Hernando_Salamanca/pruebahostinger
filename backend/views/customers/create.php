<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Customers $model */

$this->title = 'Crear Clientes';
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customers-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
