<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var backend\models\CustomersSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="customers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'rc') ?>

    <?= $form->field($model, 'category') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?php // echo $form->field($model, 'type_doc') ?>

    <?php // echo $form->field($model, 'num_doc') ?>

    <?php // echo $form->field($model, 'addres') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'eps') ?>

    <?php // echo $form->field($model, 'afp') ?>

    <?php // echo $form->field($model, 'arl') ?>

    <?php // echo $form->field($model, 'ccf') ?>

    <?php // echo $form->field($model, 'association') ?>
    
    <?php // echo $form->field($model, 'office') ?> 

    <?php // echo $form->field($model, 'user') ?>

    <?php // echo $form->field($model, 'key_system') ?>

    <?php // echo $form->field($model, 'date_register') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
