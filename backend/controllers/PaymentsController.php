<?php

namespace backend\controllers;

use common\models\Payments;
use backend\models\PaymentsSearch;
use common\models\Categories;
use common\models\Customers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentsController implements the CRUD actions for Payments model.
 */
class PaymentsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Payments models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PaymentsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDetail()
    {
        $searchModel = new PaymentsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $model_customer = Customers::find()->where(['id' => Yii::$app->request->get('id')])->one();

        return $this->render('detail', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model_customer
        ]);
    }

    /**
     * Displays a single Payments model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Payments();
        $model->date_liquidation = date('Y-m-d');
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $pension = $model->base * 0.16;  //cambiar para el 16% del valor base
                //inicio de Condicionales que asignan el valor del riesgo
                $r = 1;
                if ($model->risk_level == 1) {
                    $r = 0.00522;
                }
                if ($model->risk_level == 2) {
                    $r = 0.01044;
                }
                if ($model->risk_level == 3) {
                    $r = 0.02436;
                }
                if ($model->risk_level == 4 || $model->risk_level_2 == 4 || $model->risk_level_3 == 4) {
                    $r = 0.04350;
                }
                if ($model->risk_level == 5 || $model->risk_level_3 == 5) {
                    $r = 0.06960;
                }
                //Fin de Condicionales que asignan el valor del riesgo
                //inicio de Condicional que asigna el valor porcentual a pagar por EPS
                $cliente = Customers::find()->where(['id' => $model->customer_id])->one();
                $category = Categories::find()->where(['id' => $cliente->category])->one();
                if ($category->dependence == 1) { //dependiente
                    $percent_eps = 0.04;
                    $ccf = $model->base * 0.04;                   
                }
                if ($category->dependence == 0) { //independiente
                    $percent_eps = 0.125;
                    $ccf = $model->base * 0.02;        
                }
                //Fin de Condicional que asigna el valor porcentual a pagar por EPS
                //Inicio de Condicional que asigna el valor a pagar según factor
                if ($model->factors == 1) {
                    $model->real_value = $model->base * $percent_eps + $model->interest;
                    //$model->real_value = round($model->real_value, 3);
                    $model->paid_value = $model->real_value + $model->admin;
                }
                if ($model->factors == 2) { //Eps + R4
                    $model->real_value = $model->base * $percent_eps + ($model->base * $r) + $model->interest;
                    //$model->real_value = round($model->real_value, 3);
                    $model->paid_value = $model->real_value + $model->admin;
                }
                if ($model->factors == 3) {
                    $model->real_value = $model->base * $percent_eps + $pension + $model->interest;
                    //$model->real_value = round($model->real_value, 3);
                    $model->paid_value = $model->real_value + $model->admin;
                }
                if ($model->factors == 4) {
                    $model->real_value = ($model->base * $percent_eps) + $pension + ($model->base * $r) + $model->interest;
                    //$model->real_value = round($model->real_value, 3);
                    $model->paid_value = $model->real_value + $model->admin;
                }
                if ($model->factors == 5) {
                    $model->real_value = ($model->base * $percent_eps) + $pension + ($model->base * $r) + $ccf + $model->interest;
                    //$model->real_value = round($model->real_value, 3);
                    $model->paid_value = $model->real_value + $model->admin;
                }
                if ($model->factors == 6) {
                    $model->real_value = ($model->base * $r) + $model->interest;
                    //$model->real_value = round($model->real_value, 3);
                    $model->paid_value = $model->real_value + $model->admin;
                }
                //Fin de Condicional que asigna el valor a pagar según factor

                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Payments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payments::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
