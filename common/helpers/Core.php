<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\helpers;

use backend\models\AuthAssignment;
use backend\app\models\AuthItem;
use backend\app\models\AuthItemChild;
use common\models\Associations;
use common\models\Categories;
use common\models\Customers;
use common\models\UserConnectionBd;
use yii;
use \yii\web\Request;
use common\models\User;
use Exception;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Description of Core
 *
 * @author
 */
class Core
{

    private static $_METHOD_ENCRYPT = "AES-256-CBC";
    private static $_ITERATIONS = 999;
    private static $_SHA = "sha512";
    private static $PASSPHRASE = "wyccN1%21Srhjd@p";

    public static function getRol()
    {
        $rol = array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))[0];
        if ($rol == 'Administrador') {
            return str_replace($rol, 'Administrador', $rol);
        }
        return $rol;
    }

    public static function getRolById($id)
    {
        $rol = '';
        if ($result = Yii::$app->authManager->getRolesByUser($id)) {
            $rol = array_keys($result)[0];
        }

        return $rol;
    }

    public static function getBaseUrl()
    {
        $root = Yii::$app->request->baseUrl;
        return $root;
    }

    public static function getMessageSuccess()
    {
        Yii::$app->session->setFlash('success-swaltoast', Yii::t('app', 'Information satisfactorily modified'));
    }

    public static function getMessageClient()
    {
        Yii::$app->session->setFlash('error-swaltoast', Yii::t('app', 'Debe Agregar un Nombre de Cliente o Negocio'));
    }

    public static function getMessagePrint()
    {
        Yii::$app->session->setFlash('error-swaltoast', Yii::t('app', 'Debe Configurar la Factura'));
    }

    public static function getMessagePassword()
    {
        Yii::$app->session->setFlash('success-swaltoast', Yii::t('app', 'Revise la Bandeja De Entrada de Su Correo para Reestablecer la Contraseña'));
    }

    public static function getMessageError($message = 'Error. Try again')
    {
        return Yii::$app->session->setFlash('error-swaltoast', Yii::t('app', $message));
    }

    public static function getMessageAviso($message)
    {
        return Yii::$app->session->setFlash('warning-swaltoast', Yii::t('app', $message));
    }

    public static function getCashError($message = 'Error. Ya Tienes una Caja Abierta')
    {
        return Yii::$app->session->setFlash('error-swaltoast', Yii::t('app', $message));
    }

    public static function getBasePathUrlFrontend()
    {
        $baseUrl = str_replace('backend', 'frontend', (Yii::$app->basePath));
        return $baseUrl;
    }

    public static function getBasePathUrlBackend()
    {
        $baseUrl = str_replace('frontend', 'backend', (Yii::$app->basePath));
        return $baseUrl;
    }

    public static function getUrlFiles()
    {
        return Yii::getAlias('@staticPath') . Yii::getAlias('@static/');
    }
    public static function getFileView($name, $route)
    {
        return  $route . $name;
    }

    public static function getMessageInfo($message = 'Error. Try again')
    {
        return Yii::$app->session->setFlash('info-swaltoast', Yii::t('app', $message));
    }

    public static function sendEmailPass($user)
    {
        return Yii::$app->mailer->compose(
            ['html' => 'correito'],
            ['user' => $user]
        )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($user->email)
            ->setSubject('Registro de cuenta en ' . Yii::$app->name)
            ->send();
    }

    public static function getCategories()
    {
        return Categories::find()->all();
    }
    public static function getCustomers()
    {
        return Customers::find()->all();
    }
    public static function getAssociations()
    {
        return Associations::find()->all();
    }

}
