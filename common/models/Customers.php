<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property int $id
 * @property int $rc
 * @property int $category
 * @property string $name
 * @property string $last_name
 * @property string $type_doc
 * @property int $num_doc
 * @property string $addres
 * @property string $phone
 * @property string|null $email
 * @property string $eps
 * @property string $afp
 * @property string $arl
 * @property string $ccf
 * @property int $association
 * @property string|null $office
 * @property string $user
 * @property string $key_system
 * @property string $date_register
 *
 * @property Categories $category0
 * @property Associations $association0
 * @property Payments[] $payments 
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rc', 'category', 'name', 'last_name', 'type_doc', 'num_doc', 'addres', 'phone', 'eps', 'afp', 'arl', 'ccf', 'association', 'user', 'key_system', 'date_register'], 'required'],
            [['rc', 'category', 'num_doc', 'association'], 'integer'],
            [['date_register'], 'safe'],
            [['name', 'last_name', 'addres', 'phone', 'office', 'user', 'key_system'], 'string', 'max' => 100],
            [['type_doc', 'email', 'eps', 'afp', 'arl', 'ccf'], 'string', 'max' => 50],
            [['category'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::class, 'targetAttribute' => ['category' => 'id']],
            [['association'], 'exist', 'skipOnError' => true, 'targetClass' => Associations::class, 'targetAttribute' => ['association' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rc' => 'Rc',
            'category' => 'Categoría',
            'name' => 'Nombres',
            'last_name' => 'Apellidos',
            'type_doc' => 'Tipo Doc',
            'num_doc' => 'Num Documento',
            'addres' => 'Dirección',
            'phone' => 'Teléfono',
            'email' => 'E-mail',
            'eps' => 'Eps',
            'afp' => 'Afp',
            'arl' => 'Arl',
            'ccf' => 'Ccf',
            'association' => 'Gremio',
            'office' => 'Cargo',
            'user' => 'Usuario',
            'key_system' => 'Contraseña',
            'date_register' => 'Fecha de Registro',
        ];
    }

    /**
     * Gets query for [[Category0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory0()
    {
        return $this->hasOne(Categories::class, ['id' => 'category']);
    }

    /** 
     * Gets query for [[Association0]]. 
     * 
     * @return \yii\db\ActiveQuery 
     */
    public function getAssociation0()
    {
        return $this->hasOne(Associations::class, ['id' => 'association']);
    }

    /**
     * Gets query for [[Payments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::class, ['customer_id' => 'id']);
    }
}
