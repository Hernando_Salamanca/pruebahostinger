<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $month
 * @property int $base
 * @property string $factors
 * @property int|null $risk_level
 *  @property int|null $risk_level_2
 * @property int|null $risk_level_3
 * @property int $admin 
 * @property int $interest 
 * @property string $date_liquidation
 * @property float $real_value
 * @property float $paid_value
 * 
 * @property Customers $customer 
 */

class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'month', 'base', 'factors', 'admin', 'interest', 'date_liquidation', 'real_value', 'paid_value'], 'required'],
            [['customer_id', 'base', 'risk_level', 'risk_level_2', 'risk_level_3', 'admin', 'interest'], 'integer'],
            [['date_liquidation'], 'safe'],
            [['real_value', 'paid_value'], 'number'],
            [['month'], 'string', 'max' => 50],
            [['factors'], 'string', 'max' => 100],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::class, 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Clientes',
            'month' => 'Mes',
            'base' => 'Base',
            'factors' => 'Factor',
            'risk_level' => 'Nivel de Riesgo',
            'risk_level_2' => 'Nivel de Riesgo',
            'risk_level_3' => 'Nivel de Riesgo',
            'admin' => 'Administración',
            'interest' => 'Interés / Excedente',
            'date_liquidation' => 'Fecha de Liquidación',
            'real_value' => 'Valor Liquidado',
            'paid_value' => 'Valor Pagado',
        ];
    }
    /** 
     * Gets query for [[Customer]]. 
     * 
     * @return \yii\db\ActiveQuery 
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::class, ['id' => 'customer_id']);
    }
}
